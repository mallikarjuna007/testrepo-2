const {createProxyMiddleware}= require('http-proxy-middleware')

module.exports=function(app){
    app.use('/movieList',
    createProxyMiddleware({
        target:'https://hoblist.com',
        changeOrigin:'true',
    })
    
    )
}